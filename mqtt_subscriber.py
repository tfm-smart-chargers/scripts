import paho.mqtt.client as paho
import subprocess
import time


def on_subscribe(client, userdata, mid, granted_qos):
    print('Subscribed')

def on_message(client, userdata, msg):
    mybuffer = str(msg.payload)
    print('on_message: '+ mybuffer)
    battery_level = str(mybuffer[2]) + str(mybuffer[3]) 
    print(battery_level)
    with open("/home/pi/input_battery.txt", "w+") as myfile:
        myfile.write(battery_level + '\n')
        time.sleep(10)
        myfile.close()
    subprocess.call(['./battery_mqtt_picocom.sh'])

client = paho.Client()
client.on_subscribe = on_subscribe
client.on_message = on_message
client.connect('192.168.1.137')
client.subscribe('garage/battery_level')

client.loop_forever()
#!/bin/bash

RESULT="$(sudo alpr -c eu Pictures/placa1.jpg)"

CAR_PLATE_LINE=$(echo -e "${RESULT}" | sed -n '2p')
CAR_PLATE="$(cut -d' ' -f6 <<<${CAR_PLATE_LINE})"
echo "${CAR_PLATE}"

EUI64_EXEC="$(sh eui64.sh)"
EUI64=$(echo -e "${EUI64_EXEC}" | sed -n '2p')
EUI64=$(echo $EUI64 | sed -e 's/\r//g')
echo $EUI64
./picocom.sh $EUI64 $CAR_PLATE
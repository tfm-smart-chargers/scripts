#!/bin/bash
set -e

picocom -qrX -b 115000 /dev/ttyUSB0

echo "eui64" | picocom -qrix 1000 /dev/ttyUSB0
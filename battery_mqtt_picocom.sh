#!/bin/bash


value=$(cat input_battery.txt)
echo "battery level: "
echo $value

EUI64_EXEC="$(sh eui64.sh)"
EUI64=$(echo -e "${EUI64_EXEC}" | sed -n '2p')
EUI64=$(echo $EUI64 | sed -e 's/\r//g')
echo $EUI64

set -e
picocom -qrX -b 115000 /dev/ttyUSB0


echo "udp send 64:ff9b::34.76.91.105 4445 $EUI64#8517HYT#2#$value" | picocom -qrix 1000 /dev/ttyUSB0

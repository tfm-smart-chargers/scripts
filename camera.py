import picamera
import time
from PIL import Image

with picamera.PiCamera() as camera:
    time.sleep(5)
    camera.flash_mode = 'torch'
    camera.resolution = (2592, 1944)
    camera.capture('car_plate.jpeg', format='jpeg', bayer=True)



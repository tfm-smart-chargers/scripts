/******************************************************************************

 MQTT_battery_saver.ino

by: Ivan Tosso

  This sketch connects the ESP32 to a MQTT broker and subcribes to the topic
  garage/battery_level.
******************************************************************************/

#include <WiFi.h>
#include <PubSubClient.h>

const int sensorPin = 34;   // seleccionar la entrada para el sensor
int sensorValue;         // variable que almacena el valor raw (0 a 1023)
int connected_car = 0;
int disconnected_car = 1;
int last_saved_battery = 0;
float value;            // variable que almacena el voltaje (0.0 a 25.0)
double battery_level;
double saved_battery = 0;
char battery[100];
char last_battery[100];

const char *ssid =  "ssid";   // name of your WiFi network
const char *password =  "password"; // password of the WiFi network

const char *ID = "esp32";  // Name of our device, must be unique
const char *TOPIC = "garage/battery_level";  // Topic to subcribe to

IPAddress broker(192, 168, 100, 38); // IP address of your MQTT broker
WiFiClient wclient;

PubSubClient client(wclient); // Setup MQTT client

// Connect to WiFi network
void setup_wifi() {
  Serial.print("\nConnecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password); // Connect to network

  while (WiFi.status() != WL_CONNECTED) { // Wait for connection
    delay(500);
    Serial.print(".");
  }

  Serial.println();
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

// Reconnect to client
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(ID)) {
      Serial.println("connected");
      Serial.print("Publishing to: ");
      Serial.println(TOPIC);
      Serial.println('\n');

    } else {
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup() {
  Serial.begin(115200); // Start serial communication at 115200 baud
  setup_wifi(); // Connect to network
  client.setServer(broker, 1883);
}

void loop() {
  sensorValue = analogRead(sensorPin);          // realizar la lectura
  value = map(sensorValue, 0, 4095, 0.0, 14.8);   // cambiar escala a 0.0 – 14.0
  battery_level = value * 100 / 14.8;
  dtostrf(battery_level, 0, 0, battery);
  Serial.println("Batery level: \n"); // mostrar el valor por serial
  Serial.println(battery);
  if (!client.connected())  // Reconnect if connection is lost
  {
    reconnect();
  }
  
  if (battery_level > 0 && last_saved_battery == 0){
    disconnected_car = 0;
    connected_car = 1;
  }

  if (battery_level > saved_battery) {
    saved_battery = battery_level;
  }
  
  if (battery_level > 0 && connected_car == 1 && last_saved_battery == 0) {
    client.publish(TOPIC, battery);
    Serial.println((String)TOPIC + " => MQTT TOPIC");
    connected_car = 0;
    last_saved_battery = 1;
  }

  if (battery_level < saved_battery && disconnected_car == 0) {
    disconnected_car = 1;
  }

  if (battery_level < saved_battery && disconnected_car == 1 && last_saved_battery == 1) {
    dtostrf(saved_battery, 0, 0, last_battery);
    client.publish(TOPIC, last_battery);
    last_saved_battery = 2;        
  }
  
  client.loop();
}
******************************************************************************
  MQTT_Switch_Example.ino
  by: Ivan Tosso

  This sketch connects the ESP32 to a MQTT broker and subcribes to the topic
  garage/plate_detector. when new car was found, it sends a message to the topic
******************************************************************************/

#include <WiFi.h>
#include <PubSubClient.h>

#define trigPin 18
#define echoPin 19
#define trigger 14
#define echo 12

const char *ssid =  "ssid";   // name of your WiFi network
const char *password =  "password"; // password of the WiFi network

const char *ID = "esp32";  // Name of our device, must be unique
const char *TOPIC = "garage/plate_detector";  // Topic to subcribe to

IPAddress broker(192, 168, 100, 21); // IP address of your MQTT broker
WiFiClient wclient;

PubSubClient client(wclient); // Setup MQTT client

// Connect to WiFi network
void setup_wifi() {
  Serial.print("\nConnecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password); // Connect to network

  while (WiFi.status() != WL_CONNECTED) { // Wait for connection
    delay(500);
    Serial.print(".");
  }

  Serial.println();
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

// Reconnect to client
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(ID)) {
      Serial.println("connected");
      Serial.print("Publishing to: ");
      Serial.println(TOPIC);
      Serial.println('\n');

    } else {
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup() {
  Serial.begin(115200); // Start serial communication at 115200 baud
  setup_wifi(); // Connect to network
  client.setServer(broker, 1883);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(trigger, OUTPUT);
  pinMode(echo, INPUT);
}

void loop() {
  
  if (!client.connected())  // Reconnect if connection is lost
  {
    reconnect();
  }
  
  long duracion, distancia, duracion_2, distancia_2 ;
  digitalWrite(trigPin, LOW);        // Nos aseguramos de que el trigger está desactivado
  digitalWrite(trigger, LOW);        // Nos aseguramos de que el trigger está desactivado
  delayMicroseconds(2);              // Para asegurarnos de que el trigger esta LOW
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);             // Esperamos 10µs. El pulso sigue active este tiempo
  digitalWrite(trigPin, LOW);        // Cortamos el pulso y a esperar el echo
  duracion = pulseIn(echoPin, HIGH) ;
  distancia = duracion / 2 / 29.1  ;
  Serial.println("DISTANCE ONE " + String(distancia) + " cm.") ;
  int Limite = 200 ;                  // Medida en vacío del sensor
  int max_distance = 25;
  
  if ( distancia < max_distance) {    //En este punto comprobamos que el primer sensor encuentra un objeto cerca
    delayMicroseconds(2);              // Para asegurarnos de que el trigger esta LOW
    digitalWrite(trigger, HIGH);
    delayMicroseconds(10);             // Esperamos 10µs. El pulso sigue active este tiempo
    digitalWrite(trigger, LOW);        // Cortamos el pulso y a esperar el echo
    duracion_2 = pulseIn(echo, HIGH) ;
    distancia_2 = duracion_2 / 2 / 29.1  ;
    Serial.println("DISTANCE TWO " + String(distancia_2) + " cm.") ;
    
    if ( distancia_2 < max_distance) { 
      Serial.println("message sent");
      client.publish(TOPIC, "car inside parking");
      delay(5000);
    }
    
    else {
      delay (5000) ;                      // Para limitar el número de mediciones
    }

  }
  
  
  client.loop();
}


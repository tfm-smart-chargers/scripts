#!/bin/bash
set -e
picocom -qrX -b 115000 /dev/ttyUSB0

echo "udp send 64:ff9b::34.76.13.195 4445 $1#$2#2#50" | picocom -qrix 1000 /dev/ttyUSB0